;; -*- emacs-lisp -*-
;; From http://orgmode.org/elpa.html
(unless package-archive-contents    ;; Refresh the packages descriptions
  (package-refresh-contents))
(setq package-load-list '(all))     ;; List of packages to load
(unless (package-installed-p 'org)  ;; Make sure the Org package is
  (package-install 'org))           ;; installed, install it if not

;; From IDE tutorial
(require 'package) ;; You might already have this line
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (url (concat (if no-ssl "http" "https") "://melpa.org/packages/")))
  (add-to-list 'package-archives (cons "melpa" url) t))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))

(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)

(when (not package-archive-contents)
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; the content of custom is a customized version from https://github.com/tuhdo/emacs-c-ide-demo.
(add-to-list 'load-path "~/.emacs.d/custom")

(require 'setup-general)
(if (version< emacs-version "24.4")
    (require 'setup-ivy-counsel)
  (require 'setup-helm)
  (require 'setup-helm-gtags))
;; (require 'setup-ggtags)
;; (require 'setup-cedet)
(require 'setup-editing)

;; function-args
;; (require 'function-args)
;; (fa-config-default)
;; (define-key c-mode-map  [(tab)] 'company-complete)
;; (define-key c++-mode-map  [(tab)] 'company-complete)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("~/gitlab/my-library/bib-notes.org" "~/gitlab/my-phd-thesis/src/phd-notes.org" "~/gitlab/DAMPAT/src/dampat-notes.org" "~/gitlab/org/org-notes.org" "~/gitlab/org/contacts.org" "~/gitlab/org/from-mobile.org"))
 '(package-selected-packages
   '(gnuplot-mode org-password-manager org-pdfview multiple-cursors ace-window company-auctex company-bibtex company-c-headers company-dict company-emoji company-shell company-suggest synonymous info-colors org-vcard helm-mu ess meson-mode visual-regexp visual-regexp-steroids regex-tool pcre2el graphviz-dot-mode indent-tools org-plus-contrib org mu4e-alert mu4e-jump-to-list mu4e-maildirs-extension mu4e-query-fragments ggtags csv-nav csv csv-mode webpaste pastery paste-of-code markdown-mode markdown-mode+ dired-k irfc helm-git-grep json-mode org-mime pdf-tools ukrainian-holidays mexican-holidays calendar-norway bibretrieve helm-wordnet wordnut dictcc dictionary synosaurus bitlbee httpcode know-your-http-well ob-http google-maps google-this helm-google gscholar-bibtex fastdef org-ref google-translate htmlize vcl-mode org-cliplink org-bullets magit auctex zygospore helm-gtags helm yasnippet ws-butler volatile-highlights use-package undo-tree iedit dtrt-indent counsel-projectile company clean-aindent-mode anzu))
 '(safe-local-variable-values
   '((company-clang-arguments "-I/home/francisv/gst/master/gstreamer" "-I/home/francisv/Dropbox/glib")
     (company-clang-arguments "-I/home/francisv/gst/master" "-I/home/francisv/Dropbox/glib")))
 '(safe-local-variable-valuesr
   '((company-clang-arguments "-I/home/francisv/gst/master" "-I/home/francisv/Dropbox/glib")
     (company-clang-arguments "-I/home/francisv/gst/master/gstreamer")
     (company-clang-arguments "-I/home/francisv/gst/master/gstreamer/gst/ -I/home/francisv/gst/master/gst-plugins-base/gst/")))
 '(tramp-syntax 'default nil (tramp)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(org-babel-load-file
 (expand-file-name "~/gitlab/HOME/settings.org"
                   user-emacs-directory))
(put 'magit-clean 'disabled nil)
(put 'narrow-to-region 'disabled nil)
